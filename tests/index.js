import request from 'supertest';

import { app, bootstrap } from '../src/app';

describe('our app', () => {
  let db;

  beforeAll(async () => (db = await bootstrap()));

  beforeEach(async () => {
    await db.dropDatabase(process.env.DB_NAME + '_test');
  });

  afterAll(async () => await db.close());

  // código pt. 1
  test('GET / returns a "hello world"', async () => {
    const { body } = await request(app).get('/');

    expect(body.message).toBeDefined();
    expect(body.message).toBe('hello world');
  });

  // código pt. 2
  test('POST /projects returns a new project slug', async () => {
    const { body } = await request(app)
      .post('/projects')
      .send({ name: 'Awesome' });

    expect(body.project).toBeDefined();
    expect(body.project.slug).toBe('awesome');
  });

  // código pt.2
  test('GET /projects/:slug returns a matching project', async () => {
    await request(app).post('/projects').send({ name: 'Awesome' });

    const { body } = await request(app).get('/projects/awesome');

    expect(body.project).toBeDefined();
    expect(body.project.slug).toBe('awesome');
  });

  // código pt. 3
  test('POST /projects/:slug/boards adds a new board into a project', async () => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'todo';

    const { body } = await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name });

    expect(body.board).toBeDefined();
    expect(body.board.name).toBe(name);
  });

  //remover projeto com esse slug
  test('DELETE  /projects/:slug - remove o projeto com aquele slug', async () => {
    const slug = 'projeto1';
    await request(app).post('/projects').send({ name: slug });

    const { body } = await request(app)
      .delete(`/projects/${slug}`)
      .send({ slug });

    expect(body.slug).toBeDefined();
    expect(body.slug).toBe(slug);
  });

  //remover o board com esse nome
  test('DELETE  /projects/:slug/boards/:name - remove o board com aquele nome', async () => {
    //criação do projeto
    const project = { name: 'Awesome' };
    await request(app).post('/projects').send(project);
    const name = 'todo';
    const slug = 'awesome';
    //criação do board
    await request(app).post(`/projects/${slug}/boards`).send({ name });
    //remoção do board
    const { body } = await request(app)
      .delete(`/projects/${slug}/boards/${name}`)
      .send({ board: name, project: project });

    expect(body.board).toBeDefined();
    expect(body.board).toBe(name);
  });

  test('POST /projects/:slug/boards/:name/tasks -  adiciona uma task ao board com aquele nome', async () => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);
    const name = 'todo';
    await request(app).post(`/projects/${slug}/boards`).send({ name });

    const task = 'estudar';
    const { body } = await request(app)
      .post(`/projects/${slug}/boards/${name}/tasks`)
      .send({ task: task, project: project });

    expect(body.tasks).toBeDefined();
    expect(body.tasks).toBe(task);
  });

  test('DELETE /projects/:slug/boards/:name/tasks/:id -  remove uma task do board com aquele nome/id (da task)', async () => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);
    const name = 'todo';
    await request(app).post(`/projects/${slug}/boards`).send({ name });

    const task = 'estudar';
    await request(app)
      .post(`/projects/${slug}/boards/${name}/tasks`)
      .send({ task: task, project: project });

    const { body } = await request(app)
      .delete(`/projects/${slug}/boards/${name}/tasks/${task}`)
      .send({ project });

    expect(body.id).toBeDefined();
    expect(body.id).toBe(task);
  });
});
