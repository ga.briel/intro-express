const express = require('express');
const router = express.Router();
import { setupDB } from './db';

router.post('/', async (req, res) => {
  const db = await setupDB();
  const { name } = req.body;

  const slug = name.toLowerCase();

  const project = {
    slug,
    boards: [],
  };

  const existingProject = await db.collection('projects').findOne({ slug });

  if (existingProject) {
    res.status(400).json({ error: `project ${slug} already exists` });
  } else {
    await db.collection('projects').insertOne(project);
    res.json({ project });
  }
});

const findProject = async (req, res, next) => {
  const db = await setupDB();
  const { slug } = req.params;

  const project = await db.collection('projects').findOne({ slug });

  if (!project) {
    res.status(400).json({ error: `project ${slug} does not exist` });
  } else {
    req.body.project = project;
    // console.log(slug);
    next();
  }
};

router.get('/:slug', findProject, async (req, res) => {
  const { project } = req.body;
  res.json({ project });
});

router.post('/:slug/boards', findProject, async (req, res) => {
  const db = await setupDB();
  const { name, project } = req.body;
  const board = { name, tasks: [] };
  project.boards.push(board);

  await db
    .collection('projects')
    .findOneAndReplace({ slug: project.slug }, project);

  res.json({ board });
});

router.delete('/:slug', findProject, async (req, res) => {
  const db = await setupDB();
  const { slug } = req.body;
  const del = await db.collection('projects').deleteOne({ slug });
  if (del) {
    res.json({ slug });
  } else {
    res.status(400).json({ error: `project ${slug} does not exist` });
  }
});

router.delete('/:slug/boards/:name', findProject, async (req, res) => {
  const db = await setupDB();
  const { board, project } = req.body;
  let found = false;

  project.boards.forEach((b) => {
    if (b.name === board) {
      let index = project.boards.indexOf(board);
      project.boards.splice(index, 1);
      found = !found;
    }
  });

  if (found) {
    await db
      .collection('projects')
      .findOneAndReplace({ slug: project.slug }, project);
    res.json({ board: board });
  } else {
    res.status(400).json({ error: `board ${board} does not exist` });
  }
});

router.post('/:slug/boards/:name/tasks', findProject, async (req, res) => {
  const db = await setupDB();
  const { task, project } = req.body;
  const { name } = req.params;

  let found = false;
  project.boards.forEach((board) => {
    if (board.name === name) {
      board.tasks.push(task);
      found = !found;
    }
  });
  if (found) {
    await db
      .collection('projects')
      .findOneAndReplace({ slug: project.slug }, project);
    res.json({ tasks: task });
  } else {
    res.status(400).json({ error: `board ${name} not found` });
  }
});

router.delete(
  '/:slug/boards/:name/tasks/:id',
  findProject,
  async (req, res) => {
    const db = await setupDB();
    const { project } = req.body;
    const { name, id } = req.params;
    let del = false;
    project.boards.forEach((board) => {
      if (board.name === name) {
        let index = board.tasks.indexOf(id);
        board.tasks.splice(index, 1);
        del = true;
      }
    });
    if (del) {
      await db
        .collection('projects')
        .findOneAndReplace({ slug: project.slug }, project);
      res.json({ id });
    } else {
      res.status(400).json({ error: `item ${id} not found` });
    }
  }
);
module.exports = router;
